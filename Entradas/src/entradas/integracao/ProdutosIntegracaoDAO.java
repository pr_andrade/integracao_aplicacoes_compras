/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entradas.integracao;

import entradas.dao.Conexao;
import entradas.dao.DBErrorException;
import entradas.model.Produto;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

/**
 *
 * @author pr_andrade
 */
public class ProdutosIntegracaoDAO {
    public void inserir(Produto produto) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = Conexao.getConexao().prepareStatement(""
                    + "INSERT INTO PRODUTOS"
                    + "("
                    + "id, "
                    + "nome, "
                    + "codigo_barras, "
                    + "dat_ultima_compra, "
                    + "dat_ultima_venda, "
                    + "vlr_custo, "
                    + "vlr_venda, "
                    + "margem_lucro, "
                    + "estoque, "
                    + "fornecedores_id"
                    + ") "
                    + "VALUES("
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?"
                    + ")");

            ps.setInt(1, produto.getId());
            ps.setString(2, produto.getNome());
            ps.setString(3, produto.getCodigoBarras());
            ps.setDate(4, converteData(produto.getDatUltimaCompra()));
            ps.setDate(5, converteData(produto.getDatUltimaVenda()));
            ps.setDouble(6, produto.getCusto());
            ps.setDouble(7, produto.getVenda());
            ps.setDouble(8, produto.getMargemLucro());
            ps.setInt(9, produto.getEstoque());
            ps.setInt(10, produto.getFornecedor().getId());

            if (ps.execute()) {
                throw new DBErrorException("Ocorreu um erro durante o cadastro do Produtos. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void update(Produto produto) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = Conexao.getConexao().prepareStatement("UPDATE "
                    + "PRODUTOS SET "
                    + "nome = ?, "
                    + "codigo_barras = ?, "
                    + "dat_ultima_compra = ?, "
                    + "dat_ultima_venda = ?, "
                    + "vlr_custo = ?, "
                    + "vlr_venda = ?, "
                    + "margem_lucro = ?, "
                    + "estoque = ?, "
                    + "fornecedores_id = ?"
                    + " WHERE id = ?"
            );
            ps.setString(1, produto.getNome());
            ps.setString(2, produto.getCodigoBarras());
            ps.setDate(3, converteData(produto.getDatUltimaCompra()));
            ps.setDate(4, converteData(produto.getDatUltimaVenda()));
            ps.setDouble(5, produto.getCusto());
            ps.setDouble(6, produto.getVenda());
            ps.setDouble(7, produto.getMargemLucro());
            ps.setInt(8, produto.getEstoque());
            ps.setInt(9, produto.getFornecedor().getId());
            ps.setInt(10, produto.getId());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a atualização dos dados do Produtos. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void delete(int id) throws DBErrorException {
        PreparedStatement ps = null;

        try {            
            String query = "DELETE FROM PRODUTOS WHERE id = " + id;
            ps = Conexao.getConexao().prepareStatement(query);

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a exclusão do Produtos. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }
    
    private Date converteData(Calendar cal){
        java.util.Date date = cal.getTime();
        Date dateSQL = new Date(date.getTime());
        return dateSQL;
    }
}
