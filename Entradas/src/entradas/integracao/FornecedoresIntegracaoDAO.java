/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entradas.integracao;

import entradas.dao.Conexao;
import entradas.dao.DBErrorException;
import entradas.model.Fornecedor;
import entradas.model.ValorInvalidoException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author pr_andrade
 */
public class FornecedoresIntegracaoDAO {

    public void inserir(Fornecedor fornecedor) throws DBErrorException, ClassNotFoundException {
        PreparedStatement ps = null;

        try {
            ps = Conexao.getConexao().prepareStatement(""
                    + "INSERT INTO FORNECEDORES"
                    + "("
                    + "id ,"
                    + "razao ,"
                    + "nome_fantasia ,"
                    + "telefone ,"
                    + "celular ,"
                    + "nome_contato ,"
                    + "endereco ,"
                    + "numero ,"
                    + "bairro ,"
                    + "cidade ,"
                    + "estado ,"
                    + "cnpj ,"
                    + "inscricao ,"
                    + "dat_ultima_compra"
                    + ") "
                    + "VALUES("
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?"
                    + ")");

            ps.setString(1, String.valueOf(fornecedor.getId()));
            ps.setString(2, fornecedor.getRazao());
            ps.setString(3, fornecedor.getFantasia());
            ps.setString(4, fornecedor.getTelefone());
            ps.setString(5, fornecedor.getCelular());
            ps.setString(6, fornecedor.getContato());
            ps.setString(7, fornecedor.getEndereco());
            ps.setString(8, fornecedor.getNumero());
            ps.setString(9, fornecedor.getBairro());
            ps.setString(10, fornecedor.getCidade());
            ps.setString(11, String.valueOf(fornecedor.getEstado()));
            ps.setString(12, fornecedor.getCnpj());
            ps.setString(13, fornecedor.getInscricao());
            ps.setDate(14, converteData(fornecedor.getDatUltimaCompra()));

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante o cadastro do Fornecedores. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
                JOptionPane.showMessageDialog(null, "Fornecedores atualizados", "Atençao", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void update(Fornecedor fornecedor) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = Conexao.getConexao().prepareStatement("UPDATE "
                    + "FORNECEDORES SET "
                    + "razao = ? ,"
                    + "nome_fantasia = ? ,"
                    + "telefone = ? ,"
                    + "celular = ? ,"
                    + "nome_contato = ? ,"
                    + "endereco = ? ,"
                    + "numero = ? ,"
                    + "bairro = ? ,"
                    + "cidade = ? ,"
                    + "estado = ? ,"
                    + "cnpj = ? ,"
                    + "inscricao = ? ,"
                    + "dat_ultima_compra = ? "
                    + " WHERE id = ?"
            );
            ps.setString(1, fornecedor.getRazao());
            ps.setString(2, fornecedor.getFantasia());
            ps.setString(3, fornecedor.getTelefone());
            ps.setString(4, fornecedor.getCelular());
            ps.setString(5, fornecedor.getContato());
            ps.setString(6, fornecedor.getEndereco());
            ps.setString(7, fornecedor.getNumero());
            ps.setString(8, fornecedor.getBairro());
            ps.setString(9, fornecedor.getCidade());
            ps.setString(10, String.valueOf(fornecedor.getEstado()));
            ps.setString(11, fornecedor.getCnpj());
            ps.setString(12, fornecedor.getInscricao());
            ps.setDate(13, converteData(fornecedor.getDatUltimaCompra()));
            ps.setInt(14, fornecedor.getId());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a atualização dos dados do Fornecedores. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void delete(int id) throws DBErrorException, ClassNotFoundException {
        PreparedStatement ps = null;

        try {
            String query = "DELETE FROM FORNECEDORES WHERE id = ?";
            ps = Conexao.getConexao().prepareStatement(query);

            ps.setInt(1, id);

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a exclusão do Fornecedores. Entre em contato com o administrador.");
            } else {
                Conexao.getConexao().commit();
            }

        } catch (SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public Fornecedor buscaFornecedoresPorId(int id) throws DBErrorException, ClassNotFoundException, ValorInvalidoException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String query = "SELECT * FROM FORNECEDORES WHERE id = " + id;
            ps = Conexao.getConexao().prepareStatement(query);

            rs = ps.executeQuery(query);

            while (rs.next()) {
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setId(rs.getInt("id"));
                fornecedor.setRazao(rs.getString("razao"));
                fornecedor.setFantasia(rs.getString("nome_fantasia"));
                fornecedor.setTelefone(rs.getString("telefone"));
                fornecedor.setCelular(rs.getString("celular"));
                fornecedor.setContato(rs.getString("nome_contato"));
                fornecedor.setEndereco(rs.getString("endereco"));
                fornecedor.setNumero(rs.getString("numero"));
                fornecedor.setBairro(rs.getString("bairro"));
                fornecedor.setCidade(rs.getString("cidade"));
                fornecedor.setEstado(rs.getString("estado"));
                fornecedor.setCnpj(rs.getString("cnpj"));
                fornecedor.setInscricao(rs.getString("inscricao"));
                fornecedor.setDatUltimaCompra(converteDataSQL(rs.getDate("dat_ultima_compra")));
            }

        } catch (SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return null;
    }

    private Date converteData(Calendar cal) {
        java.util.Date date = cal.getTime();
        Date dateSQL = new Date(date.getTime());
        return dateSQL;
    }

    public Calendar converteDataSQL(Date dataSQL) {
        if (dataSQL == null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(Date.from(Instant.now()));
            return cal;
        }
        Calendar cal = new GregorianCalendar();
        cal.setTime(dataSQL);
        return cal;
    }
}
