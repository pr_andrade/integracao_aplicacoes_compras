package entradas.integracao;

import entradas.controller.ControladorProduto;
import entradas.dao.DBErrorException;
import entradas.model.Fornecedor;
import entradas.model.Produto;
import entradas.model.ValorInvalidoException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 
 * @author pr_andrade
 */
public class InicializadorDeIntegracao {

    private final FornecedoresIntegracaoDAO fornecedoresIntegracao;
    private final ProdutosIntegracaoDAO produtosIntegracao;

    public InicializadorDeIntegracao() {
        this.produtosIntegracao = new ProdutosIntegracaoDAO();
        this.fornecedoresIntegracao = new FornecedoresIntegracaoDAO();
    }

    protected void buildObject(String path, String linha, String operacao) throws IOException, ClassNotFoundException, ParseException, DBErrorException, ValorInvalidoException {
        //System.out.println("Path: " + path + "\nLinha: " + linha + "\nOperação: " + operacao);
        String[] objetoParticionado = linha.split(";");

        if (path.contains("fornecedores.csv")) {
            Fornecedor fornecedor = new Fornecedor();
            fornecedor.setId(Integer.parseInt(objetoParticionado[1]));
            fornecedor.setRazao(objetoParticionado[2]);
            fornecedor.setFantasia(objetoParticionado[3]);
            fornecedor.setTelefone(objetoParticionado[4]);
            fornecedor.setCelular(objetoParticionado[5]);
            fornecedor.setContato(objetoParticionado[6]);
            fornecedor.setEndereco(objetoParticionado[7]);
            fornecedor.setNumero(objetoParticionado[8]);
            fornecedor.setBairro(objetoParticionado[9]);
            fornecedor.setCidade(objetoParticionado[10]);
            fornecedor.setEstado(objetoParticionado[11]);
            fornecedor.setCnpj(objetoParticionado[12]);
            fornecedor.setInscricao(objetoParticionado[13]);
            fornecedor.setDatUltimaCompra(converteDataDoCSV(objetoParticionado[14]));
            

            switch (operacao) {
                case "0":
                    fornecedoresIntegracao.inserir(fornecedor);
                    break;
                case "1":
                    fornecedoresIntegracao.update(fornecedor);
                    break;
                case "2":
                    fornecedoresIntegracao.delete(fornecedor.getId());
                    break;
            }
        } else if (path.contains("produtos.csv")) {
            System.out.println("Path: " + path + "\nLinha: " + linha + "\nOperação: " + operacao);
            Produto produto = new Produto();
            produto.setId(Integer.parseInt(objetoParticionado[1]));
            produto.setNome(objetoParticionado[2]);
            produto.setCodigoBarras(objetoParticionado[3]);
            produto.setDatUltimaCompra(converteDataDoCSV(objetoParticionado[4]));
            produto.setDatUltimaVenda(converteDataDoCSV(objetoParticionado[5]));
            produto.setCusto(Double.parseDouble(objetoParticionado[6]));
            produto.setVenda(Double.parseDouble(objetoParticionado[7]));
            produto.setMargemLucro(Double.parseDouble(objetoParticionado[8]));
            produto.setEstoque(Integer.parseInt(objetoParticionado[9]));
            produto.setFornecedor(buscaFornecedorId(Integer.parseInt(objetoParticionado[10])));

            switch (operacao) {
                case "0":
                    produtosIntegracao.inserir(produto);
                    break;
                case "1":
                    produtosIntegracao.update(produto);
                    break;
                case "2":
                    produtosIntegracao.delete(produto.getId());
                    break;
            }
        }
    }
    
    public Calendar converteDataDoCSV(String dataDoCSV) throws ParseException {
        if(dataDoCSV.equals("null")){
            Calendar cal = new GregorianCalendar();
            cal.setTime(Date.from(Instant.now()));
            return cal;
        }        
        
        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZ yyyy", Locale.US);
        Date parsedDate = (Date) df.parse(dataDoCSV);
        
        Calendar cal = new GregorianCalendar();
        cal.setTime(parsedDate);
        
        return cal;
    }

    private Fornecedor buscaFornecedorId(int codigo) throws DBErrorException, ClassNotFoundException, ValorInvalidoException {
        FornecedoresIntegracaoDAO fiDAO = new FornecedoresIntegracaoDAO();
        return fiDAO.buscaFornecedoresPorId(codigo);
    }
}
